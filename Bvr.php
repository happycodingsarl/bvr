<?php

namespace GenerateBVR;

/**
 *  Generating a BVR without amount
 *
 * @see https://gist.github.com/gbaudoin/9066c1a3d6ab68351635
 */
class Bvr {

  /**
   * Post Bank Account
   * @var string
   */
  private $bankAccount;

  /**
   * Invoice Number (7 digits)
   * @var string
   */
  private $invoiceNumber;

  /**
   * Customer Number (7 digits)
   * @var string
   */
  private $customerNumber;

  /**
   * Customer Number (1 digit)
   * 0 = invoice, 1 to 4 = reminders
   * @var string
   */
  private $subject;

  /**
   * BVR Reference Number
   * @var
   */
  private $BvrReferenceNumber = '';

  /**
   * Table for digit key calculation
   * @var array
   */
  private static $table = [
    [0,9,4,6,8,2,7,1,3,5],
    [9,4,6,8,2,7,1,3,5,0],
    [4,6,8,2,7,1,3,5,0,9],
    [6,8,2,7,1,3,5,0,9,4],
    [8,2,7,1,3,5,0,9,4,6],
    [2,7,1,3,5,0,9,4,6,8],
    [7,1,3,5,0,9,4,6,8,2],
    [1,3,5,0,9,4,6,8,2,7],
    [3,5,0,9,4,6,8,2,7,1],
    [5,0,9,4,6,8,2,7,1,3]
  ];

  /**
   * Creates a BVR instance.
   *
   */
  public function __construct($bankAccount = '010562302', $invoiceNumber = '0005101', $customerNumber = '0002072', $subject = '0') {
    $this->bankAccount = $bankAccount;
    $this->invoiceNumber = $invoiceNumber;
    $this->customerNumber = $customerNumber;
    $this->subject = $subject;
  }

  /**
   * Calcul du chiffre-clé en PHP, modulo 10, récursif BVR Suisse
   * Berechnung der Prüfziffer nach Modulo 10, rekursiv BESR Schweiz
   * Calculating the Check Digits Using Module 10, Recursively
   *
   * @param $number
   * @return int
   */
  private function digitKeyCalculation($number) {
    $report = 0;
    foreach(str_split($number) as $key => $value) {
      $report = self::$table[$report][(int)$value];
    }

    return (10 - $report) % 10;
  }

  /**
   * Validate the digit key
   *
   * @param $number
   * @param $digit
   * @return bool
   */
  public static function validate($number, $digit) {
    return $digit == self::digitKeyCalculation($number);
  }

  /**
   * Set & return the reference number
   * Depends on your accounting system
   *
   * @return string
   */
  public function referenceNumber() {
    $this->BvrReferenceNumber = $value = $this->invoiceNumber . $this->customerNumber . $this->subject . $this->digitKeyCalculation($this->invoiceNumber . $this->customerNumber . $this->subject);

    // Format number by adding spaces
    $i = 1;
    $formattedValue = '';
    $max = strlen($this->BvrReferenceNumber);
    $chars = str_split($this->BvrReferenceNumber);
    while ( $i <= $max) {
      $formattedValue = $chars[$max - $i] . $formattedValue;
      if (is_int($i / 5)) {
        $formattedValue = ' ' . $formattedValue;
      }
      $i++;
    }

    return $formattedValue;
  }

  /**
   * Get BVR Code for a BVR without amount
   *
   * @return string
   */
  public function getBVRCodeWithoutAmount() {
    if (empty($this->BvrReferenceNumber)) {
      $this->referenceNumber();
    }

    return '042>' . $this->BvrReferenceNumber . '+ ' . $this->bankAccount . '>';
  }
}
